use std::env;

pub struct Config {
  pub db: DbConfig,
}

pub struct DbConfig {
  pub host: String,
  pub port: String,
  pub user: String,
  pub password: String,
}

impl Config {
  pub fn new() -> Config {
    Config {
      db: DbConfig {
        host: env::var("DB_HOST").unwrap_or("localhost".to_string()),
        port: env::var("DB_PORT").unwrap_or("5432".to_string()),
        user: env::var("DB_USER").unwrap_or("postgres".to_string()),
        password: env::var("DB_PASSWORD").unwrap_or("postgres".to_string()),
      },
    }
  }
}
