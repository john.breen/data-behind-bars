use actix_web::{get, App, HttpResponse, HttpServer, Result};
use data_behind_bars::{config, db};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Health {
    version: String,
}

#[get("/__health__")]
async fn health_check() -> Result<HttpResponse> {
    Ok(HttpResponse::Ok().json(Health {
        version: env!("CARGO_PKG_VERSION").to_string(),
    }))
}

#[get("/facilities")]
async fn get_facilities() -> Result<HttpResponse> {
    // TODO inject these, you fool
    let config = config::Config::new();
    let mut client = db::connect(config.db).unwrap();
    
    let facilities = db::get_facilities(&mut client).unwrap();

    Ok(HttpResponse::Ok().json(facilities))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(health_check).service(get_facilities))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}
