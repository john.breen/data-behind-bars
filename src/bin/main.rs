extern crate data_behind_bars;
use data_behind_bars::{config, db, facility};

use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let fac = facility::read_facilities()?;
    let config = config::Config::new();
    let mut client = db::connect(config.db)?;

    db::create_table(&mut client)?;
    for f in fac {
        db::add_facilities(&mut client, f)?;
    }

    Ok(())
}
