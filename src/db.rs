use postgres::{Client, Error, NoTls, Row};
use std::fmt;
use std::str::FromStr;

use crate::config::DbConfig;
use crate::facility::{Facility, FacilityType};

#[derive(Debug)]
pub struct DbError {
    message: String,
}

impl fmt::Display for DbError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Ah shit, it broke in the db layer")
    }
}
impl From<Error> for DbError {
    fn from(error: Error) -> Self {
        DbError {
            message: format!("{:?}", error),
        }
    }
}

impl std::error::Error for DbError {}

// TODO Maybe make this a struct? get it from the env
pub fn connect(config: DbConfig) -> Result<Client, DbError> {
    let client = Client::connect(
        &format!(
            "host={} port={} user={} password={}",
            config.host, config.port, config.user, config.password
        ),
        NoTls,
    )?;

    Ok(client)
}

pub fn create_table(client: &mut Client) -> Result<(), DbError> {
    client.batch_execute(
        "CREATE TABLE IF NOT EXISTS facility (
                    id SERIAL PRIMARY KEY,
                    name VARCHAR NOT NULL,
                    address VARCHAR NOT NULL,
                    city VARCHAR NOT NULL,
                    state VARCHAR NOT NULL,
                    postal_code VARCHAR NOT NULL,
                    phone VARCHAR NOT NULL,
                    fax VARCHAR,
                    facility_type VARCHAR NOT NULL)",
    )?;

    Ok(())
}

pub fn add_facilities(client: &mut Client, facility: Facility) -> Result<(), DbError> {
    &client.execute(
        "INSERT INTO facility (name, address, city, state, postal_code, phone, fax, facility_type)
                            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
        &[
            &facility.name,
            &facility.address,
            &facility.city,
            &facility.state,
            &facility.postal_code,
            &facility.phone,
            &facility.fax,
            &facility.facility_type.to_string(),
        ],
    )?;

    Ok(())
}

impl From<&Row> for Facility {
    fn from(row: &Row) -> Self {
        Facility {
            name: row.get("name"),
            address: row.get("address"),
            city: row.get("address"),
            state: row.get("address"),
            postal_code: row.get("address"),
            phone: row.get("phone"),
            fax: row.get("fax"),
            facility_type: FacilityType::from_str(row.get("facility_type")).unwrap(),
        }
    }
}

pub fn get_facilities(client: &mut Client) -> Result<Vec<Facility>, DbError> {
    let results = client.query("SELECT * from facility", &[])?;
    let facilities: Vec<Facility> = results
        .iter()
        .map(|result| Facility::from(result))
        .collect();

    Ok(facilities)
}
