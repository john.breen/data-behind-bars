use serde::{Deserialize, Serialize};
use serde_json;
use std::error::Error;
use std::fmt;
use std::fs;
use std::str::FromStr;

#[derive(Debug, Deserialize, Serialize)]
pub enum FacilityType {
    Prison,
    Jail,
    ImmigrantDetention,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Facility {
    pub name: String,
    pub address: String,
    pub city: String,
    pub state: String,
    pub postal_code: String,
    pub phone: String,
    pub fax: Option<String>,
    pub facility_type: FacilityType,
}

impl fmt::Display for FacilityType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl FromStr for FacilityType {
    type Err = ();

    fn from_str(input: &str) -> Result<FacilityType, Self::Err> {
        match input {
            "Jail" => Ok(FacilityType::Jail),
            "ImmigrantDetention" => Ok(FacilityType::ImmigrantDetention),
            "Prison" => Ok(FacilityType::ImmigrantDetention),
            _ => Err(()),
        }
    }
}

pub fn read_facilities() -> Result<Vec<Facility>, Box<dyn Error>> {
    let file = fs::read_to_string("src/facilities.json")?;

    let fac: Vec<Facility> = serde_json::from_str(&file.to_owned())?;
    return Ok(fac);
}
