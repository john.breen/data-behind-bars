```mermaid
erDiagram
    facility ||--o{ person : incarcerates
    person ||--o{ sentence : sentenced
    facility {
        string name
        string address
        string city
        string state
        string postalCode
        string phone
        string fax
        enum facilityType
    }

    person {
        string name
        string dob
    }

    sentence {
        list charges
        int length
        date start
    }
```

